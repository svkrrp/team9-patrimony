# Patrimony



## Getting started

To run this app follow below steps - 
1. Make sure to setup your metamask wallet beforehand
2. Clone the app and go to frontend directory
3. Do `npm ci` on terminal
4. Do `npm start` on terminal
5. Visit http://localhost:3000/ to see the app


## Name
Patrimony - Simplifies buying and selling of land hassle free

## Description
Patrimony
In the above project we have aimed at reducing corruption, fraud and misue of law which happens in residential property business. It is not easy to buy a home for a middle class person and in many case they get to know that the land they were living in is under government jurisdictions. These case occur again and again. Through blockchain security and smart contracts we can solve all above problems

## Visuals

Home Page
![Home!](images/HomeNew.jpg)

Registration
![Registration!](images/FilledRegistration.png)
![Registering!](images/FilledRegistrationFormLoading.png)

Metamask Wallet
![Transaction!](images/wallet.png)

Details page
![Details!](images/Details.png)
![Buying a Land!](images/Buying.png)
![After Buying!](images/AfterBuy.png)

## License
This project is created for NPCI Blockchain hackathon
