export const ABI = [
    {
      inputs: [],
      stateMutability: "nonpayable",
      type: "constructor",
      signature: "constructor",
    },
    {
      inputs: [],
      name: "authority",
      outputs: [{ internalType: "address", name: "", type: "address" }],
      stateMutability: "view",
      type: "function",
      constant: true,
      signature: "0xbf7e214f",
    },
    {
      inputs: [
        { internalType: "uint256", name: "landIndex", type: "uint256" },
        { internalType: "uint256", name: "idx", type: "uint256" },
      ],
      name: "getPreviousLandOwners",
      outputs: [
        {
          components: [
            { internalType: "string", name: "name", type: "string" },
            { internalType: "address", name: "addr", type: "address" },
          ],
          internalType: "struct Patrimony.LandOwner",
          name: "",
          type: "tuple",
        },
      ],
      stateMutability: "nonpayable",
      type: "function",
      signature: "0xc4dd2477",
    },
    {
      inputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      name: "lands",
      outputs: [
        { internalType: "uint256", name: "landId", type: "uint256" },
        { internalType: "address", name: "currentOwner", type: "address" },
        {
          components: [
            { internalType: "string", name: "firstLineAddress", type: "string" },
            { internalType: "string", name: "secondLineAddress", type: "string" },
            { internalType: "string", name: "city", type: "string" },
            { internalType: "string", name: "state", type: "string" },
            { internalType: "uint256", name: "pincode", type: "uint256" },
          ],
          internalType: "struct Patrimony.Address",
          name: "addr",
          type: "tuple",
        },
        { internalType: "uint256", name: "landOwnersLength", type: "uint256" },
        { internalType: "string", name: "imageUrl", type: "string" },
      ],
      stateMutability: "view",
      type: "function",
      constant: true,
      signature: "0xe261f1e5",
    },
    {
      inputs: [],
      name: "landsCount",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      stateMutability: "view",
      type: "function",
      constant: true,
      signature: "0x1c9afd0f",
    },
    {
      inputs: [
        { internalType: "string", name: "name", type: "string" },
        { internalType: "string", name: "addrLine1", type: "string" },
        { internalType: "string", name: "addrLine2", type: "string" },
        { internalType: "string", name: "city", type: "string" },
        { internalType: "string", name: "state", type: "string" },
        { internalType: "uint256", name: "pincode", type: "uint256" },
        { internalType: "uint256", name: "landId", type: "uint256" },
        { internalType: "string", name: "imgUrl", type: "string" },
      ],
      name: "registerLand",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
      signature: "0x8cbbcae2",
    },
    {
      inputs: [
        { internalType: "address", name: "landOwnerAddr", type: "address" },
        { internalType: "string", name: "name", type: "string" },
      ],
      name: "transfer",
      outputs: [],
      stateMutability: "payable",
      type: "function",
      payable: true,
      signature: "0x09886649",
    },
  ];
  
  export const ABI_ADDRESS = "0xB0dBf0760Cbcd53F6476fBC095605b96f72aFE73";
  
  export const DEFAULT_LAND_IAMGE_URL = "https://media.istockphoto.com/id/1254330782/photo/aerial-view-of-land-and-positioning-point-area.jpg?s=612x612&w=0&k=20&c=oHjL4W2hwndoS_0-nnagiJYgsDfYcZJtgfPYvVmKWp4=";
  
  export const INCORRECT_VALUE_FOR_LAND_IMAGE_URL = ['INDIA']