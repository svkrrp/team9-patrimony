import React, { useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './components/Home';
import Buy from './components/Buy';
import Register from './components/Register';
import './App.css';
import Nav from './components/Nav';
import Details from './components/Details';

const abi = [{"inputs":[],"stateMutability":"nonpayable","type":"constructor","signature":"constructor"},{"inputs":[],"name":"authority","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function","constant":true,"signature":"0xbf7e214f"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"lands","outputs":[{"internalType":"uint256","name":"landId","type":"uint256"},{"internalType":"address","name":"currentOwner","type":"address"},{"components":[{"internalType":"string","name":"firstLineAddress","type":"string"},{"internalType":"string","name":"secondLineAddress","type":"string"},{"internalType":"string","name":"city","type":"string"},{"internalType":"string","name":"state","type":"string"},{"internalType":"uint256","name":"pincode","type":"uint256"}],"internalType":"struct Patrimony.Address","name":"addr","type":"tuple"},{"internalType":"string","name":"imageUrl","type":"string"}],"stateMutability":"view","type":"function","constant":true,"signature":"0xe261f1e5"},{"inputs":[],"name":"landsCount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function","constant":true,"signature":"0x1c9afd0f"},{"inputs":[{"internalType":"string","name":"addrLine1","type":"string"},{"internalType":"string","name":"addrLine2","type":"string"},{"internalType":"string","name":"city","type":"string"},{"internalType":"string","name":"state","type":"string"},{"internalType":"uint256","name":"pincode","type":"uint256"},{"internalType":"uint256","name":"landId","type":"uint256"},{"internalType":"string","name":"imgUrl","type":"string"}],"name":"registerLand","outputs":[],"stateMutability":"nonpayable","type":"function","signature":"0xb7e735ac"},{"inputs":[{"internalType":"address","name":"landOwnerAddr","type":"address"}],"name":"transfer","outputs":[],"stateMutability":"payable","type":"function","payable":true,"signature":"0x1a695230"}];

function App() {
  useEffect(() => {
    test()
  }, []);

  const test = async () => {
    // const patrimony = new web3.eth.Contract(abi, "0xB95fa2176Efc723F4348ea70E6C5E9aE53aEf41a");
    // const accounts = await web3.eth.getAccounts();
    // await patrimony.methods.registerLand("surya madir mod", "godhna road", "arrah", "bihar", 802302, 6329294, "https://media.istockphoto.com/id/1297391904/photo/scenic-view-of-wheat-field.jpg?b=1&s=170667a&w=0&k=20&c=ZPMhRzGsW3pWu7zC_NDcbCjp8Lt1tWNXbIzzyEzOeJ0=").send({
    //   from: accounts[0]
    // })
    // get
    // const address = await patrimony.methods.authority().call();
    // console.log(address)
    // const transact = await patrimony.methods.pincodes(802302, 0).call()
    console.log("done")
  }

  return (
    <div>
      <Nav />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/register" element={<Register />}></Route>
          <Route path="/buy" element={<Buy />}></Route>
          <Route path="/details/:id" element={<Details />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
