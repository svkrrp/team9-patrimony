// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

contract Patrimony {
    struct Address {
        string firstLineAddress;
        string secondLineAddress;
        string city;
        string state;
        uint pincode;
    }
    struct LandOwner {
        string name;
        address addr;
    }
    struct Land {
        uint landId;
        address currentOwner;
        Address addr;
        mapping(uint => LandOwner) previousLandOwners;
        uint landOwnersLength;
        string imageUrl;
    }
    address public authority;
    mapping(uint => Land) public lands;
    uint public landsCount;

    modifier restricted() {
        require(msg.sender == authority);
        _;
    }

    constructor() {
        authority = msg.sender;
    }

    function registerLand(
        string memory name,
        string memory addrLine1,
        string memory addrLine2,
        string memory city,
        string memory state,
        uint pincode,
        uint landId,
        string memory imgUrl) public {
        Address memory landAddress = Address({
            firstLineAddress: addrLine1,
            secondLineAddress: addrLine2,
            city: city,
            state: state,
            pincode: pincode
        });

        lands[landsCount].landId = landId;
        lands[landsCount].addr = landAddress;
        lands[landsCount].currentOwner = msg.sender;
        lands[landsCount].landOwnersLength = 1;
        lands[landsCount].previousLandOwners[0] = LandOwner({
            name: name,
            addr: msg.sender
        });
        lands[landsCount].imageUrl = imgUrl;

        landsCount++;
    }

    function getPreviousLandOwners(uint landIndex, uint idx) public returns (LandOwner memory) {
        return lands[landIndex].previousLandOwners[idx];
    }

    function transfer(address landOwnerAddr, string memory name) public payable {
        for(uint i=0; i<landsCount; i++) {
            if (lands[i].currentOwner == landOwnerAddr) {
                LandOwner memory newLandOwner = LandOwner({
                    name: name,
                    addr: msg.sender
                });
                lands[i].previousLandOwners[lands[i].landOwnersLength] = newLandOwner;
                lands[i].landOwnersLength++;
                lands[i].currentOwner = msg.sender;
                payable(landOwnerAddr).transfer(msg.value);
                break;
            }
        }
    }
}